/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import modelo.Area;
import modelo.Area.AreaTipo;
import modelo.Carro;
import modelo.CarroPrototipo;
import modelo.FichaServicios;
import modelo.Servicio;

/**
 *
 * @author andrs1294
 */
public class CtrlSistema {

    Queue<FichaServicios> autosAtendidos;
    Area pintura;
    Area mecEspecializada;
    Area mecGeneral;
    static CtrlSistema ctrlSistema;
    CarroPrototipo carroPrototipo;

    private CtrlSistema() {
        autosAtendidos = new LinkedList<>();
        pintura = new Area(Area.AreaTipo.pintura);
        mecEspecializada = new Area(Area.AreaTipo.mecEspecializada);
        mecGeneral = new Area(Area.AreaTipo.mecGeneral);
        carroPrototipo = new CarroPrototipo();
    }

    public static CtrlSistema getControlador() {
        if (ctrlSistema == null) {
            ctrlSistema = new CtrlSistema();
        }
        return ctrlSistema;

    }

    public void registrarAuto(String placa, Color color, String modelo, AreaTipo area, Servicio[] servicios) throws CloneNotSupportedException {
        Carro carro = carroPrototipo.getPrototipo();
        carro.setColor(color);
        carro.setModelo(modelo);
        carro.setPlaca(placa);
        FichaServicios registro = new FichaServicios(carro, servicios, area);
        registrarServicio(registro, area);
    }

    private Servicio[] casteoManual(Object[] ser) {
        if (ser != null) {
            Servicio[] aux = new Servicio[ser.length];

            for (int i = 0; i < ser.length; i++) {
                aux[i] = (Servicio) ser[i];
            }
            return aux;
        }
        return null;
    }

    /**
     * Obteniene los servicios de una area dada
     * @param area Area de la cual se quiere obtener sus servicios
     * @return Un arreglo con los servicios
     */
    public Servicio[] getServicios(AreaTipo area) {
        Object retorno[] = null;
        if (pintura.getArea() == area) {
            retorno = pintura.getServicios().toArray();
        } else if (mecEspecializada.getArea() == area) {
            retorno = mecEspecializada.getServicios().toArray();
        } else if (mecGeneral.getArea() == area) {
            retorno = mecGeneral.getServicios().toArray();
        }
        return casteoManual(retorno);
    }

    public Queue<FichaServicios> getAutosPorRecaudar() {

        return autosAtendidos;
    }

    public void registrarCarroAtendido(FichaServicios car) {
        autosAtendidos.add(car);
    }

    public Object getAutosPorAtender(AreaTipo area) {
        if (pintura.getArea() == area) {
            return pintura.getEnEspera();
        } else if (mecEspecializada.getArea() == area) {
            return mecEspecializada.getEnEspera();
        } else if (mecGeneral.getArea() == area) {
            return mecGeneral.getEnEspera();
        }
        return null;
    }

    private void registrarServicio(FichaServicios registro, AreaTipo area) {
        if (pintura.getArea() == area) {
            pintura.agregar(registro);
        } else if (mecEspecializada.getArea() == area) {
            mecEspecializada.agregar(registro);
        } else if (mecGeneral.getArea() == area) {
            mecGeneral.agregar(registro);
        }
    }

}
