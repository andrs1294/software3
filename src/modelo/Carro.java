/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.awt.Color;
import java.awt.Rectangle;

/**
 *
 * @author andresG1294

 */
public class Carro implements Cloneable{

    private String placa;
    private String modelo;
    private Color color;
    static final int alto=70;
    static final int ancho=40;
    private Rectangle area;
    private Color colorDestino; 


    public Carro(String placa, String modelo, Color color) {
        this.placa = placa;
        this.modelo = modelo;
        this.color = color;
        area = new Rectangle(ancho, alto);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(Color color) {
        this.color = color;
    }

    public Rectangle getArea() {
        return area;
    }

    /**
     * @return the colorDestino
     */
    public Color getColorDestino() {
        return colorDestino;
    }

    /**
     * @param colorDestino the colorDestino to set
     */
    public void setColorDestino(Color colorDestino) {
        this.colorDestino = colorDestino;
    }

    
  
    
    
    
    
}
