/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import control.CtrlSistema;
import java.awt.Rectangle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andrs1294
 */
public class animacionHilo extends Thread {

    FichaServicios car;
    animacion panel;
    CtrlSistema control;
    Rectangle pin;

    public animacionHilo(FichaServicios ser, animacion ani,Rectangle pintura) {

        this.car = ser;
        this.panel = ani;
        control=CtrlSistema.getControlador();
        this.start();
        this.pin=pintura;
        System.out.println("Se crea");
    }

    @Override
    public void run() {
        Servicio serv[] = car.getServicios();
        Servicio servTotal[] = control.getServicios(car.getArea());
        for (int i = 0; i < servTotal.length; i++) {
            if(i==0)
            {
                animarTransicion(0, servTotal.length);
                pin.setLocation(car.getCarro().getArea().x, car.getCarro().getArea().y);
            }
            if(servicioRequerido(serv,servTotal[i]))
            {
                animarEstadia(servTotal[i]);
            }
            if(i+1<serv.length)
            {
                animarTransicion(i+1,servTotal.length);
            }
            
        }
        
        panel.animacionTerminada();
        System.out.println("Se elimina");
    }

    private boolean servicioRequerido(Servicio[] servCarro, Servicio servTotal) {
        for (Servicio serv1 : servCarro) {
            if(servTotal==serv1)
            {
                return true;
            }
        }
        return false;
    }

    private void animarEstadia(Servicio servTotal) {
        try {
            if(car.getArea()==Area.AreaTipo.pintura)
            {
                panel.graficarPintado();
                
                int tiempo = servTotal.getTiempo();
                int espaciado = Carro.alto/60;
                
                System.out.println("Comienza pintura");
                for (int i = 0; i < 60; i++) {
                    pin.setLocation(pin.x, car.getCarro().getArea().y-espaciado);
                    pin.setSize(pin.width, espaciado*(i+1));
                    panel.repaint();
                    Thread.sleep(6000/60);
                }
                //restante
                int faltante=Carro.alto-pin.height;
                for (int i = 0; i < faltante; i++) {
                    pin.setLocation(pin.x, car.getCarro().getArea().y-1);
                    pin.setSize(pin.width, pin.height+1);
                    panel.repaint();
                    Thread.sleep(6000/60);
                }
            }else
            {
                Thread.sleep(servTotal.getTiempo()*100);
            }
            
        } catch (InterruptedException ex) {
            Logger.getLogger(animacionHilo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void animarTransicion(int i,int cantidadServi) {
       Rectangle area = car.getCarro().getArea();
       int espaciadoY=panel.getHeight()/cantidadServi;
       int posYFinal = espaciadoY*(cantidadServi-(i+1));
       int diferencia = (espaciadoY*(cantidadServi-(i)) - posYFinal );
       posYFinal+=(diferencia-Carro.alto)/2;
       while(area.y>posYFinal)
       {
           area.setLocation(area.x, area.y-5);
           panel.repaint();
           try {
               Thread.sleep(100);
           } catch (InterruptedException ex) {
               Logger.getLogger(animacionHilo.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
       
    }

  
}
