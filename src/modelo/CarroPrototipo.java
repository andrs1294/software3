/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.awt.Color;

/**
 *
 * @author cata
 */
public class CarroPrototipo {
 
    Carro proto;

    public CarroPrototipo() {
        proto= new Carro("123", "1994", Color.yellow);
    }
    
    
    public Carro getPrototipo() throws CloneNotSupportedException{
        
        return (Carro)proto.clone();
    }
    
    
    
}
