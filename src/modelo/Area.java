/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Queue;

/**
 *
 * @author cata
 */
public class Area {
    private AreaTipo area;
    private LinkedList<Servicio> servicios;
    private Object enEspera;

    private void establecerTipoDato() {
        if(this.area == AreaTipo.mecEspecializada)
        {
            enEspera = new LinkedList<FichaServicios>();
        }else if(this.area == AreaTipo.mecGeneral)
        {
            enEspera = new Stack<FichaServicios>();
        }else if(this.area == AreaTipo.pintura)
        {
            enEspera = new LinkedList<FichaServicios>();
        }
    }

    public static enum AreaTipo
    {
        mecGeneral("Mec. General"),pintura("Pintura"),mecEspecializada("Mec. Especializada");
        
        private String nombre;
        
        private AreaTipo(String nom) {        
            this.nombre=nom;
        }

        @Override
        public String toString() {
            return nombre; //To change body of generated methods, choose Tools | Templates.
        }
        
        
    }
  
    public Area(AreaTipo nombre) {
        this.area = nombre;
        this.servicios = new LinkedList<>();
        
        llenarServicios();
        establecerTipoDato();
    }

    /**
     * Este metodo permite llenar los servicios dependiendo del area
     */
    public void llenarServicios()
    {
        if(this.area == AreaTipo.mecEspecializada)
        {
            servicios.add(new Servicio("Reparacion Motor", 350000.0, 15));
            servicios.add(new Servicio("Rev. y reparación eléctrica", 2000000.0, 60));
        }else if(this.area == AreaTipo.mecGeneral)
        {
            servicios.add(new Servicio("Revision de frenos", 200000.0, 10));
            servicios.add(new Servicio("Cambio de Aceite", 40000.0, 10));
            servicios.add(new Servicio("Alineacion", 50000.0, 10));
            servicios.add(new Servicio("Balanceo", 35000.0, 10));
        }else if(this.area == AreaTipo.pintura)
        {
            servicios.add(new Servicio("Pintura", 2000000.0, 60));
        }
    }

    public AreaTipo getArea() {
        return area;
    }
  
    

    /**
     * @return the servicios
     */
    public LinkedList<Servicio> getServicios() {
        return servicios;
    }

    /**
     * @param servicios the servicios to set
     */
    public void setServicios(LinkedList<Servicio> servicios) {
        this.servicios = servicios;
    }

    @Override
    public String toString() {
       
        String retorno="";
        if(area == AreaTipo.mecEspecializada)
        {
            retorno= "Mec. Especializada";
        }else if(area == AreaTipo.mecGeneral)
        {
            retorno= "Mec. General";
        }else if(area == AreaTipo.pintura)
        {
            retorno= "Pintura";
        }
        
        return  retorno;
    }

    public Object getEnEspera() {
        return enEspera;
    }

    
    public void agregar(FichaServicios ser)
    {
         if(area == AreaTipo.mecEspecializada || area == AreaTipo.pintura)
        {
            ((Queue<FichaServicios>)(enEspera)).add(ser);
        }else if(area == AreaTipo.mecGeneral)
        {
            ((Stack<FichaServicios>)(enEspera)).push(ser);
        }
    }
  
    
    
}
