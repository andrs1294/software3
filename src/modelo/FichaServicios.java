/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import modelo.Area.AreaTipo;


/**
 *
 * @author cata
 */
public class FichaServicios {
    private Carro carro;
    private Servicio[] servicios;
    private AreaTipo area;

   

    public FichaServicios(Carro carro, Servicio servicios[],AreaTipo area) {
        this.carro = carro;
        this.servicios = servicios;
        this.area=area;
    }

    public AreaTipo getArea() {
        return area;
    }

    
    public float getCostoTotal()
    {
        float costo = 0;
        
        for (Servicio servicio : servicios) {
            if(servicio==null)
            {
                break;
            }
            costo+=servicio.getCosto();
        }
        
        return costo;
    }
    /**
     * @return the carro
     */
    public Carro getCarro() {
        return carro;
    }

    /**
     * @param carro the carro to set
     */
    public void setCarro(Carro carro) {
        this.carro = carro;
    }

    public Servicio[] getServicios() {
        return servicios;
    }

   
    
}
